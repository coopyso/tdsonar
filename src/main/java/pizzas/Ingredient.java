package pizzas;

import java.util.Objects;

public class Ingredient {

    private String nom;
    private boolean vegetarien;

    public String getNom() {
        return nom;
    }

    public boolean isVegetarien() {
        return vegetarien;
    }

    public Ingredient(String nom, boolean vegetarien) {
        this.nom = nom;
        this.vegetarien = vegetarien;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ingredient that = (Ingredient) o;
        return Objects.equals(nom, that.nom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nom);
    }

}
